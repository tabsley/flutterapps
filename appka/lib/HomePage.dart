import 'package:flutter/material.dart';
import 'dart:math';

class HomePage extends StatefulWidget {
  @override
  HomePageState createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  static const Color color = Colors.white;
  final random = Random();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Odświeżacz'),
      ),
      body: new MyRotatedPic(),
    );
  }
}

class MyRotatedPic extends StatefulWidget {
  MyRotatedPic({Key key}) : super(key: key);

  @override
  MyRotatedPicState createState() {
    return MyRotatedPicState();
  }
}

class MyRotatedPicState extends State<MyRotatedPic> {
  double rotationFactor = 0.0;

  void increaseRotationFactor() {
    setState(() {
      rotationFactor += 0.30;
    });
  }

  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: increaseRotationFactor,
      child: Container(
          child: Transform.rotate(
              angle: rotationFactor,
              child: Align(
                  alignment: Alignment.center,
                  child: (Image.asset(
                    'images/m200.png',
                    width: 300,
                    fit: BoxFit.cover,
                  ))))),
    );
  }
}
