import 'package:flutter/material.dart';
import 'package:appka/HomePage.dart';

main() => runApp(Appka());
class Appka extends StatelessWidget {
  static const String _title = 'Hello World';


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: _title,
      theme: ThemeData(),
      home: HomePage(),

    );
  }
}
